#!/bin/python3
import os, glob
for envKey in os.environ:
    needReplace = False
    if (envKey[:4] == 'PHP_'):
        cfgKey = envKey[4:]
        cfgVal = os.environ[envKey]
        cfgsearchDir = '/etc/php/**/*.ini'
        cfgCommentChar = ';'
        cfgKeyValSeparator = ' = '
        needReplace = True
    elif (envKey[:7] == 'APACHE_'):
        cfgKey = envKey[7:]
        cfgVal = os.environ[envKey]
        cfgsearchDir = '/etc/apache2/**/*.conf'
        cfgCommentChar = '#'
        cfgKeyValSeparator = ' '
        needReplace = True

    if (needReplace):
        print('Find env ' + envKey + ' = ' + cfgVal)
        print('Find string "' + cfgKey + '" in configuration files ...')
        for cfgFile in glob.glob(cfgsearchDir, recursive=True):
            with open(cfgFile) as f:
                cfgContent = f.read()
            f.close()
            if (cfgKey in cfgContent):
                print('Find string "' + cfgKey + '" in configuration file ' +
                      cfgFile)
                with open(cfgFile) as f:
                    cfgLines = f.readlines()
                f.close()
                os.remove(cfgFile)
                f = open(cfgFile, 'w')
                lineNumb = 1
                for cfgLine in cfgLines:
                    cfgLine = cfgLine.strip()
                    x1 = cfgKey
                    x2 = cfgCommentChar + x1
                    if ((cfgLine[:len(x1)] == x1)
                            or (cfgLine[:len(x2)] == x2)):
                        print('Replacing configuration in ' + cfgFile + ':' +
                              str(lineNumb))
                        newCfgLine = cfgKey + cfgKeyValSeparator + cfgVal + "\n"
                    else:
                        newCfgLine = cfgLine + "\n"
                    f.write(newCfgLine)
                    lineNumb = lineNumb + 1
                f.close()